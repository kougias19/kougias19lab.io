#include <EEPROM.h>
#include <CmdParser.hpp>

CmdParser cmdParser;

int ledPin = 13;
int directionPin = 8;
int pinPumpa = 13;
char type = 0;

unsigned long counter = 0;
int delayMicrosecond = 500; //initial speed
bool bGo = false;

float coefficient = 0; // steps per micro liter

// positions in EEPROM
int startingWithData = 10;
int typePosition = 3;
int coefficientPosition = 5;


void setup()
{
  Serial.begin(9600);
  Serial.println("Modul Open pH stat");

  pinMode(directionPin, OUTPUT); // direction
  pinMode(9, OUTPUT);

  digitalWrite(directionPin, HIGH);

  // Set command seperator.
  cmdParser.setOptSeperator('|');

  readModuleType();
  readCoefficient();
}

void loop()
{
  if (bGo) {
    makeOneStep();
  }

  if (Serial.available()) {
    // use own buffer from serial input
    CmdBuffer<300> myBuffer;

    // Read line from Serial until timeout
    if (myBuffer.readFromSerial(&Serial, 30000)) {
      if (cmdParser.parseCmd(&myBuffer) != CMDPARSER_ERROR) {
        if (cmdParser.equalCmdParam(0, "A")) {
          a();
        }
        else if (cmdParser.equalCmdParam(0, "B")) {
          b();
        }
        else if (cmdParser.equalCmdParam(0, "ADD")) { // in micro liters
          String volume = cmdParser.getCmdParam(1);
          long lvolume = volume.toInt();
          
          if (lvolume > 0) {
            digitalWrite(ledPin, HIGH);
            if (type == '1') { //peristaltic
              digitalWrite(pinPumpa, HIGH);
              //Serial.println(lvolume / coefficient/1000);
              delay(lvolume / coefficient/1000);
              digitalWrite(pinPumpa, LOW);
            }
            else if ( type = '2') { //syringe
              digitalWrite(directionPin, LOW);
              for (long i = 0; i < coefficient * lvolume; i ++) {
                makeOneStep();
              }
            }
            digitalWrite(ledPin, LOW);
          }
        }
        else if (cmdParser.equalCmdParam(0, "LOAD")) { // in micro liters
          String volume = cmdParser.getCmdParam(1);
          long lvolume = volume.toInt();
          Serial.println(lvolume);
          if (lvolume > 0) {
            digitalWrite(ledPin, HIGH);
            digitalWrite(directionPin, HIGH);
            for (long i = 0; i < coefficient * lvolume; i ++) {
              makeOneStep();
            }
            digitalWrite(ledPin, LOW);
          }
        }
        else if (cmdParser.equalCmdParam(0, "M")) {
          String time = cmdParser.getCmdParam(1);
          if (time.toInt() > 0) {
            digitalWrite(ledPin, HIGH);
            digitalWrite(pinPumpa, HIGH);
            delay(time.toInt());
            digitalWrite(ledPin, LOW);
            digitalWrite(pinPumpa, LOW);
          }
        }
        else if (cmdParser.equalCmdParam(0, "T")) {
          Serial.println("TEST");
        }
        else if (cmdParser.equalCmdParam(0, "G")) {
          readModulSettings();
        }
        else if (cmdParser.equalCmdParam(0, "W")) {
          writeModulSettings();
        }
        else if (cmdParser.equalCmdParam(0, "SET_COEFFICIENT")) {
          writeCoefficient();
        }
        else if (cmdParser.equalCmdParam(0, "GET_COEFFICIENT")) {
          readCoefficient();
          Serial.println(coefficient, 8);
        }
        else if (cmdParser.equalCmdParam(0, "SET_TYPE")) {
          writeModulType();
        }
        else if (cmdParser.equalCmdParam(0, "GET_TYPE")) {
          readModuleType();
          Serial.println(type);
        }
        else if (cmdParser.equalCmdParam(0, "STOP")) {
          bGo = false;
        }
        else if (cmdParser.equalCmdParam(0, "GO")) {
          bGo = true;
        }
        else if (cmdParser.equalCmdParam(0, "PUNI")) {
          digitalWrite(directionPin, HIGH);
        }
        else if (cmdParser.equalCmdParam(0, "PRAZNI")) {
          digitalWrite(directionPin, LOW);
        }
        else if (cmdParser.equalCmdParam(0, "S")) {
          String del = cmdParser.getCmdParam(1);
          delayMicrosecond = del.toInt();
        }
        else {
          Serial.println("Parser error!");
        }
      }
    }
  }
}

void makeOneStep() {
  digitalWrite(9, HIGH);
  //delay(1);
  delayMicroseconds(delayMicrosecond);
  digitalWrite(9, LOW);
  delayMicroseconds(delayMicrosecond);
  //counter++;
  //Serial.println(counter);
}

void readModulSettings() {
  byte lengthOfString;

  EEPROM.get(0, lengthOfString);
  //Serial.println("Ucitava se " + String(lengthOfString));

  for (int i = 0; i < lengthOfString ; i++)
  {
    char c = EEPROM.read( i + startingWithData);
    Serial.print(c);
  }
  Serial.println();
}

void writeModulSettings() {
  int eeAddress = 0;   //Location we want the data to be put.
  char name[100];

  String nam = cmdParser.getCmdParam(1);
  byte lengthOfString = nam.length();
  nam.toCharArray(name, 100);
  Serial.println(cmdParser.getCmdParam(1));
  if (lengthOfString > 1) {
    EEPROM.put(0, lengthOfString);
    for (int i = 0; i < lengthOfString ; i++)
    {
      EEPROM.write( i + startingWithData, nam[i] );
      delay(100);
    }
    Serial.println("Upisano " + String(lengthOfString));
  }
}

void writeModulType() {
  String par = cmdParser.getCmdParam(1);
  byte type = par[0];
  EEPROM.write(typePosition, type);
}

void readModuleType() {
  char c = EEPROM.read(typePosition);
  type = c;
}


void a() {
  digitalWrite(13, HIGH);
}

void b() {
  digitalWrite(13, LOW);
}

void writeCoefficient() {
  String coefS = cmdParser.getCmdParam(1);
  float coef = coefS.toFloat();
  EEPROM.put(coefficientPosition, coef);
}

void readCoefficient() {
  EEPROM.get(coefficientPosition, coefficient);
  if (coefficient == 0) {
    Serial.println("Deffault value for coefficient");
    if (type == '1') { //peristaltic
      coefficient = 0.00056;
    }
    else if ( type = '2') { //syringe
      coefficient = 6.3512;
    }
  }
  //Serial.println(coefficient);
}
