package rs.mpele.pHStat;

import java.util.ArrayList;
import com.fazecast.jSerialComm.SerialPort;

public class ArduinoCore {

	private ArrayList<String> mPortovi;
	private SerialPort mComPort;
	private double mKoeficijentPumpe; // ml/ mil sec
	private String[] settings;

	public static void main(String[] args) throws InterruptedException {
		ArduinoCore arduino = new ArduinoCore();
		try {
			arduino.konektujSe(SerialPort.getCommPorts()[1].getSystemPortName());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		arduino.testKonekcije();
		//		arduino.komanda("G");
		//		arduino.procitaj();
		System.out.println("Coefficient: "+arduino.getKoeficijentPumpe());
		arduino.clearBuffer();
		//arduino.setKoeficijentPumpe(1.2345);
		//		arduino.readModulSettings();
		System.out.println("Coefficient: "+arduino.getKoeficijentPumpe());

		//		System.out.print("broj portova: "+Array.getLength(SerialPort.getCommPorts())+"\n");
		//		for(SerialPort por : SerialPort.getCommPorts()){			
		//			System.out.println("----");
		//			System.out.println(por.getDescriptivePortName());
		//			System.out.println(por.getSystemPortName());
		//		}
		//		for(String nazivPorta : arduino.ucitajPortove()){
		//			System.out.println(nazivPorta);
		//		}
		arduino.disconect();
	}

	public ArrayList<String> ucitajPortove(){
		mPortovi = new ArrayList<String>();

		for(SerialPort por : SerialPort.getCommPorts()){
			mPortovi.add(por.getSystemPortName());
		}

		return mPortovi;
	}

	/**
	 * Konektovanje na prvi slobodan port
	 * @throws InterruptedException 
	 */
	public void konektujSe() throws InterruptedException{
		int brojPorta=0; 
		konektujSe(SerialPort.getCommPorts()[brojPorta].getSystemPortName());
	}

	/**
	 * konektuje se na izabrani port
	 * @param port
	 * @return 
	 * @throws InterruptedException 
	 */
	public boolean konektujSe(String port) throws InterruptedException {
		if(mComPort != null && mComPort.isOpen()){
			//ako je vec otvoren isti port ne otvara ga ponovo
			if(mComPort.getSystemPortName().equals(port)){
				System.out.println("It is allready connected to port "+port);
				return true;
			}
			else{
				//ako je otvoren drugi port prvo ga zatvara
				System.out.println("Closing port: "+mComPort.getSystemPortName());
				mComPort.closePort();
				konektujSe(port);
			}
		}

		mComPort = SerialPort.getCommPort(port);
		mComPort.setBaudRate(9600);
		System.out.println("Connecting to port: "+port+" - "+mComPort.getDescriptivePortName());

		if(mComPort.openPort()){
			long curTime = System.currentTimeMillis();
			byte[] newData = new byte[100];

			//wait for intro message of modul
			while (mComPort.bytesAvailable() == 0){  
				Thread.sleep(20);
				if(curTime + 6*1000 < System.currentTimeMillis()) 
					return false;
			}
			mComPort.readBytes(newData, mComPort.bytesAvailable()); 
			//TODO check intro message of module

			Thread.sleep(300);
			testKonekcije();
			komanda("GET_COEFFICIENT");
			String procitano = procitaj();
			System.out.println("Coefficient: "+procitano+"*");

			// TODO !!!!!!!!!!!!!! Dodati zastitu za prvo koriscenje arduino plocice
			setKoeficijentPumpe(Double.valueOf(procitano));

			return true;
		}
		else {
			return false;
		}

		//		return mComPort.openPort();			
	}


	public String procitaj(){
		String output = "";
		long curTime = System.currentTimeMillis();
		try {
			while (true)
			{
				while (mComPort.bytesAvailable() == 0)
					if(curTime + 5*1000 < System.currentTimeMillis()) {					
						Thread.sleep(20);

						System.out.println("Read timeout");
						return null;
					}

				byte[] newData = new byte[mComPort.bytesAvailable()];
				int numRead = mComPort.readBytes(newData, newData.length);

				output += new String(newData);
				System.out.println("Read " + numRead + " bytes: ");
				if(output.contains("\n")){
					System.out.println(output);
					return output.substring(0, output.length()-2);
				}

			}
		} catch (Exception e) { e.printStackTrace(); }

		return null;
	}

	public void komanda(String komanda){
		byte[] arduinoKomanda = (komanda+"\n").getBytes();
		mComPort.writeBytes(arduinoKomanda , arduinoKomanda.length);
		System.out.println("ArduinoCore: "+komanda);
	}

	/**
	 * Doziranje pumpom odredjeni broj milisekundi
	 * @param miliSekundi
	 */
	public void dozirajMiliSekundi(long miliSekundi){
		komanda("M|"+String.valueOf(miliSekundi));
	}

	/**
	 * Doziranje pumpom odredjeni broj ml
	 * @param miliSekundi
	 */
	public void dozirajVolumeMl(double volume){
		komanda("ADD|"+String.valueOf(volume*1000));
	}

	public void doziranjeUkljuci(){
		komanda("A");
	}

	public void doziranjeIskljuci(){
		komanda("B");
	}


	public void disconect(){
		mComPort.closePort();		
	}

	public boolean testKonekcije() {
		if(mComPort == null)
			return false;

		if(!mComPort.isOpen())
			return false;

		//proverava koliko je bajtova dostupno u baferu
		int brBajtova = mComPort.bytesAvailable();
		int numRead =0;
		if(brBajtova > 0){
			//prazni buffer
			byte[] newData = new byte[brBajtova];
			numRead = mComPort.readBytes(newData, newData.length);
			System.out.println("In buffer were " + numRead + " bytes.");
		}

		//salje komandu za test
		komanda("T");

		//ceka odgovor
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// proverava da li je Arduino rekao "TEST"
		byte[] newData2 = new byte[mComPort.bytesAvailable()];
		numRead = mComPort.readBytes(newData2, newData2.length);
		String part = new String(newData2);
		System.out.println("Read " + numRead + " bytes.");
		System.out.println(part);

		if(part.contains("TEST"))
			return true;
		else
			return false;
	}


	/**
	 * Preracunava koliko se dadaje mililitra dodato za definisano vreme
	 * @param milisekundi
	 * @return
	 */
	public Double mS2mL(long milisekundi){
		return mKoeficijentPumpe*milisekundi;
	}

	public long mL2mS(Double volume) {
		return (long) (volume/mKoeficijentPumpe);
	}

	public double getKoeficijentPumpe() {
		return mKoeficijentPumpe;
	}

	public void setKoeficijentPumpe(double coefficientPump){
		this.mKoeficijentPumpe = coefficientPump;
	}

	public void saveCoefficientPump(double coefficientPump) {
		komanda("SET_COEFFICIENT|" + String.format ("%f", coefficientPump));
	}

	/**
	 * Read data stored i eeprom of module
	 * @return
	 * @throws InterruptedException
	 */
	public String readModulSettings() throws InterruptedException{
		byte[] newData = new byte[100];
		String result = "";
		komanda("G");
		long curTime = System.currentTimeMillis();

		while(true){
			while (mComPort.bytesAvailable() == 0){
				Thread.sleep(20);
				if(curTime + 5*1000 < System.currentTimeMillis()) {
					System.out.println("Read module timeout");
					return "Read module timeout";
				}
			}

			int numRead = mComPort.readBytes(newData, newData.length);
			result = result+ (new String(newData)).substring(0, numRead);
			if(result.contains("\\")){
				settings = result.substring(0, result.length()-4).split(";");
				return result;
			}
		}
	}

	public void clearBuffer() throws InterruptedException{
		Thread.sleep(30);
		byte[] newData = new byte[100];
		mComPort.readBytes(newData, newData.length);
	}

	public void saveModulSettings(String settings){
		komanda("W|"+settings+"\\\\");
	}

	public ModuleType getModuleType() throws InterruptedException{
		if(settings == null)
			readModulSettings();
		return ModuleType.valueOf(settings[Preferences.TYPE.getId()]);
	}

	public String getModuleName() {
		return settings[Preferences.NAME.getId()];
	}

	public String getModuleDescription(){
		return settings[Preferences.DESCRIPTION.getId()];
	}

	public static String prepareModulSettings(ModuleType type, String name, String description){
		String separator = ";";
		return type.name()+separator+name+separator+description;
	}

	public void setModuleType(ModuleType moduleType) {
		komanda("SET_TYPE|"+moduleType.getId());
	}
}

