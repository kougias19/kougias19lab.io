package rs.mpele.pHStat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GraphTimeFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	/** The time series data. */
	TimeSeries seriesPH;
	TimeSeries seriesJedincnoDodavanje;

	/**
	 * faktor kojim se uvecava dodata kolicina da bi stala na grafik
	 */
	private double mFaktorZaJedinicnoDodavanje = 21;

	private JFreeChart mJFreeChart; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GraphTimeFrame frame = new GraphTimeFrame();
					frame.setVisible(true);
					frame.ucitajFajl("rezultati2017.07.11.txt");
					//frame.ucitajFajlDodateZapremine("rezultati2017.07.11.txt");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GraphTimeFrame() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);  
		setBounds(630, 100, 450, 300);
		setTitle("Graph");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		this.seriesPH = new TimeSeries("pH");
		final TimeSeriesCollection datasetPH = new TimeSeriesCollection(this.seriesPH);

		this.seriesJedincnoDodavanje = new TimeSeries("Dosing");
//		final TimeSeriesCollection datasetJedinicnoDodavanje = new TimeSeriesCollection(this.seriesJedincnoDodavanje);
		
		datasetPH.addSeries(seriesJedincnoDodavanje);

		final JFreeChart chart = createChart(datasetPH);

		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setMouseWheelEnabled(true);

		contentPane.add(chartPanel);
	}

	private JFreeChart createChart(final XYDataset dataset) {
		mJFreeChart = ChartFactory.createTimeSeriesChart(
				null, 
				"Time", 
				"pH/Dosing",
				dataset, 
				true, 
				true, 
				false
				);
		final XYPlot plot = mJFreeChart.getXYPlot();
		ValueAxis axis = plot.getDomainAxis();
		axis.setAutoRange(true);
		//axis.setFixedAutoRange(60000.0*10);  // 60 seconds *10
		axis = plot.getRangeAxis();
		axis.setRange(5.0, 14.0);
		//plot.setRangeAxis(1, axis);

		plot.setBackgroundPaint(Color.white);
		
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(Color.BLACK);

		plot.setDomainGridlinesVisible(true);
		plot.setDomainGridlinePaint(Color.BLACK);

		return mJFreeChart;
	}

	public void dodajVrednost(Date vreme, double zapremina){
		RegularTimePeriod p = new Millisecond(vreme);
		seriesPH.add(p, zapremina);
	}
	
	public void dodajVrednosti(Date vreme, double pH, double jedinicnoDodavanje){
		RegularTimePeriod p = new Millisecond(vreme);
		seriesPH.addOrUpdate(p, pH);
		seriesJedincnoDodavanje.addOrUpdate(p, jedinicnoDodavanje*mFaktorZaJedinicnoDodavanje);
	}

	public void dodajVrednosti(String izlazPodaci) throws ParseException {
		if(izlazPodaci == null){
			return;
		}
		
		String[] vrednosti = izlazPodaci.split(",");
		//System.out.println(vrednosti[1]);

		SimpleDateFormat standardDateFormat = new SimpleDateFormat("dd.MM HH.mm.ss.S");
		Date vreme = standardDateFormat.parse(vrednosti[0]);
		//System.out.println(vrednosti[0] + "*" + vreme);

		dodajVrednosti(vreme, Double.valueOf(vrednosti[1]), Double.valueOf(vrednosti[3]));
	}

	public void dodajVrednostiDodataZapremina(String izlazPodaci) throws ParseException {
		String[] vrednosti = izlazPodaci.split(",");

		SimpleDateFormat standardDateFormat = new SimpleDateFormat("dd.MM HH.mm.ss.S");
		Date vreme = standardDateFormat.parse(vrednosti[0]);
		//System.out.println(vrednosti[0] + "*" + vreme);

		dodajVrednost(vreme, Double.valueOf(vrednosti[4]));
	}
	
	public void ucitajFajl(String nazivFajla) throws FileNotFoundException, IOException, ParseException{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		try (BufferedReader br = new BufferedReader(new FileReader(nazivFajla))) {
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				dodajVrednosti(line);
			}
		}
	}
	
	public void ucitajFajlDodateZapremine(String nazivFajla) throws FileNotFoundException, IOException, ParseException{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		mJFreeChart.getXYPlot().getRangeAxis().setLabel("Total volume, ml");
		try (BufferedReader br = new BufferedReader(new FileReader(nazivFajla))) {
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				dodajVrednostiDodataZapremina(line);
			}
		}
	}
	
	public void setNaslov(String naslov){
		setTitle(naslov);
	}

	public void resetPrikaza() {
		XYPlot plot = mJFreeChart.getXYPlot();
		ValueAxis axis = plot.getDomainAxis();
		axis.setAutoRange(true);
		axis.setFixedAutoRange(60000.0*10);  // 60 seconds *10
		axis = plot.getRangeAxis();
		axis.setRange(9.0, 13.0);		
	}

}
