package rs.mpele.pHStat;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import javax.swing.BorderFactory;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class GraphVolumeFrame extends JFrame {

	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GraphVolumeFrame frame = new GraphVolumeFrame();
					frame.setVisible(true);
					frame.ucitajFajlDodateZapremine("rezultati2017.06.22.txt");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private XYSeries series;

	public GraphVolumeFrame() {

		initUI();
	}

	private void initUI() {

		XYDataset dataset = createDataset();
		JFreeChart chart = createChart(dataset);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 15, 15));
		chartPanel.setBackground(Color.white);
		add(chartPanel);

		pack();
		setTitle("Graph pH/Volume");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(630, 100, 450, 300);

	}

	private XYDataset createDataset() {
		series = new XYSeries("");

		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(series);

		return dataset;
	}

	private JFreeChart createChart(XYDataset dataset) {

		JFreeChart chart = ChartFactory.createXYLineChart(
				null, 
				"Volume, ml", 
				"pH", 
				dataset, 
				PlotOrientation.VERTICAL,
				true, 
				true, 
				false 
				);

		XYPlot plot = chart.getXYPlot();

		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		renderer.setSeriesPaint(0, Color.RED);
		renderer.setSeriesStroke(0, new BasicStroke(2.0f));

		plot.setRenderer(renderer);
		plot.setBackgroundPaint(Color.white);

		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(Color.BLACK);

		plot.setDomainGridlinesVisible(true);
		plot.setDomainGridlinePaint(Color.BLACK);

		//chart.getLegend().setFrame(BlockBorder.NONE);

		//chart.setTitle(new TextTitle("Average Salary per Age", new Font("Serif", java.awt.Font.BOLD, 18)));

		return chart;
	}

	void addData(double x, double y){
		series.add(x, y);
	};

	public void ucitajFajlDodateZapremine(String nazivFajla) throws FileNotFoundException, IOException, ParseException{
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		try (BufferedReader br = new BufferedReader(new FileReader(nazivFajla))) {
			String line;
			while ((line = br.readLine()) != null) {
				System.out.println(line);
				dodajVrednostiDodataZapremina(line);
			}
		}
	}

	public void dodajVrednostiDodataZapremina(String izlazPodaci) throws ParseException {
		if(izlazPodaci == null)
			return;

		String[] vrednosti = izlazPodaci.split(",");

		System.out.println(Double.valueOf(vrednosti[2]) + " " + Double.valueOf(vrednosti[4]));

		addData(Double.valueOf(vrednosti[4]), Double.valueOf(vrednosti[1]));
	}

}