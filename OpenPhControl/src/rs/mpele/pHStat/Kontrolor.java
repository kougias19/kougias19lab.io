package rs.mpele.pHStat;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JLabel;

public class Kontrolor {


	long lastTime;
	private double mCiljaniPH = 10.55;
	private double mErrSum, mLastErr;
	private double kp, ki, kd;
	/** Koncentracia NaOH (mol/dm3)	 */
	private double mKoncentracijaNaOH;
	private double mPoslednjeDodataZapremina = 0;

	/** Ima vrednost 0 ako nema obrade	 */
	private long mVremeStartovanjaDoziranja;
	private long mVremePoslednjeObrade;
	private long mDodavanoOdPoslednjeObrade_mS = 0;

	private long mAutomatskoDodavanjeDo = 0;

	/** za fiksno dodavanje	 */
	private long vremePoslednjegDoziranja = 0;
	private double mJedinicnoFiksnoDodavanje_mL = 2.0;

	private boolean mAutomatskoPodesavanjeParametara;
	private JLabel mFiksnoDodavanjeLabel;
	private JLabel mVremeIzmedjDodavanjaLabel;
	private WorkingModeEnum mWorkingMode;
	private double mLastUsedPh = -1;
	private long mTimePhChanged = 0;

	
	/** Zapremina suda u dm3 */
	protected double mZapreminaSuda;
	protected ArduinoCore mArduinoCore;
	protected double mUkupnoDodatoMililitara = 0;
	protected boolean mRucniRezimRada = false;
	protected long mMinimalnoVremeIzmedjuDvaDodavanja_s = 5;
	protected String mIzlazPodaci;
	protected String mNazivUzorka;
	private Double mTitrationDose = 0.5;
	
	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {

		ArduinoCore arduinoCore = new ArduinoCore();
		arduinoCore.konektujSe();

		Kontrolor kontrolor = new Kontrolor(arduinoCore);
		kontrolor.SetTunings(1, 0, 0);
		kontrolor.setCiljaniPH(10.55);
		kontrolor.setZapreminaSuda(5);
		kontrolor.setKoncentracijaNaOH(0.5);
		System.out.println(kontrolor.izracunajKorekcijuUmL(10.4));
	}

	public double getPoslednjeDodataZapremina(){
		return mPoslednjeDodataZapremina;
	}

	/**
	 *  dodaje mililitara u automatskom razimu rada
	 * @param mililitara
	 * @return 
	 */
	long addML(double mililitara) {
		mArduinoCore.dozirajVolumeMl(mililitara);
		mUkupnoDodatoMililitara += mililitara;
		mPoslednjeDodataZapremina = mililitara;

		long brMiliSekundi = (long)(mililitara/mArduinoCore.getKoeficijentPumpe());
		mAutomatskoDodavanjeDo = System.currentTimeMillis() + brMiliSekundi;
		mVremeStartovanjaDoziranja = System.currentTimeMillis();
		System.out.println(mAutomatskoDodavanjeDo +" "+ System.currentTimeMillis() +" "+ brMiliSekundi);
		return brMiliSekundi;
	}


	public Kontrolor(ArduinoCore arduinoCore) throws FileNotFoundException, IOException {
		super();
		this.mArduinoCore = arduinoCore;

		Podesavanja podesavanja = new Podesavanja();
		podesavanja.ucitajPodesavanja();

		// podrazumevani naziv uzorka - naziv fajla
		mNazivUzorka = "rezultati"+new SimpleDateFormat("yyyy.MM.dd").format(new Date())+".txt";
	}

	public void setKoncentracijaNaOH(double koncentracijaNaOH) {
		mKoncentracijaNaOH = koncentracijaNaOH;	
	}

	public Double getCiljaniPh() {
		return mCiljaniPH;
	}

	public Double getJedinicnoFiksnoDodavanje_mL() {
		return mJedinicnoFiksnoDodavanje_mL;
	}

	public void setJedinicnoFiksnoDodavanje_mL(double mJedinicnoFiksnoDodavanje_mL) {
		this.mJedinicnoFiksnoDodavanje_mL = mJedinicnoFiksnoDodavanje_mL;
	}

	public void setCiljaniPH(double ciljaniPH) {
		mCiljaniPH = ciljaniPH;
	}

	public void SetTunings(double Kp, double Ki, double Kd) {
		kp = Kp;
		ki = Ki;
		kd = Kd;
	}

	/**
	 * Racuna za koliko treba menjati pH
	 * @param ocitanPH
	 * @return
	 */
	private double izracunajRelativnuKorekcijuPH(double ocitanPH)
	{
		if(ocitanPH>mCiljaniPH){
			return 0;
		}

		/*How long since we last calculated*/ 
		//dovoljno je precizno za ovaj slucaj
		long now = System.currentTimeMillis(); //System.nanoTime();

		double timeChange = (double)(now - lastTime);

		/*Compute all the working error variables*/
		double error = mCiljaniPH - ocitanPH;
		mErrSum += (error * timeChange);
		double dErr = (error - mLastErr) / timeChange;

		/*Compute PID Output*/
		double output = kp * error + ki * mErrSum + kd * dErr;

		/*Remember some variables for next time*/
		mLastErr = error;
		lastTime = now;

		return output;
	}

	/**
	 * Vraca zapreminu u ml
	 * @param ocitanPH
	 * @return
	 */
	public double izracunajKorekcijuUmL(double ocitanPH) {
		double relativnaKorekcija = izracunajRelativnuKorekcijuPH(ocitanPH); // bezdimienziona jedinica (pH)
		System.out.println("ocitanPH: "+ocitanPH);
		System.out.println("mCiljaniPH: "+mCiljaniPH);
		System.out.println("relativnaKorekcija: "+relativnaKorekcija);

		double koncentracijaH = Math.pow(10, -ocitanPH); // trenutna koncentracija H+ jona u rastvoru (mol/dm3)
		double ciljanaKoncentracijaH = Math.pow(10, -(ocitanPH + relativnaKorekcija)); // koncetracija H+ jona u rastvoru posle korekcije (mol/dm3)

		System.out.println("koncetracijaH:"+ koncentracijaH);
		System.out.println("Korekcija H+ za:"+ (koncentracijaH - ciljanaKoncentracijaH));
		double zapreminaNaOh = mZapreminaSuda*(koncentracijaH - ciljanaKoncentracijaH)/mKoncentracijaNaOH; // u dm3
		System.out.println("zapremina NaOH:"+ zapreminaNaOh*1000+" mL");

		return zapreminaNaOh*1000;
	}

	
	public void obradi(double ocitanPH){
		switch (mWorkingMode) {
		case PH_STAT:
			obradi_PH_Stat(ocitanPH);
			break;
		case TITRATION:
			obradiTitration(ocitanPH);
			break;
		default:
			System.err.println(" !!!! nije definisan mod rada !!!!!");
			break;
		}
	}


	private void obradiTitration(double ocitanPH) {
		double oldValue = mUkupnoDodatoMililitara;
		long currentTime = System.currentTimeMillis();

		if(Math.abs(mLastUsedPh - ocitanPH) < 0.001){
			if(mTimePhChanged + 5 *1000 < currentTime){
				addML(mTitrationDose);
				mTimePhChanged  = currentTime;
			}
			mLastUsedPh = ocitanPH;
		}
		else{
			System.out.println("menja vreme ");
			mLastUsedPh = ocitanPH;
			mTimePhChanged  = currentTime;
		}
		
		// writes to file (graph) only after adding reagents
		if(oldValue != mUkupnoDodatoMililitara){ 
			snimiUFajl(ocitanPH, 0, 1., mUkupnoDodatoMililitara, "nacin dodavanja");
		}

	}

	private void obradi_PH_Stat(double ocitanPH){
		long trenutnoVreme = System.currentTimeMillis();
		Double dodatoOdPoslednjeObrade_mL = 0.;

		if(!mRucniRezimRada){ // automatski rezim rada
			//obradjuje 

			//	double potrebnaZapreminaNaOH = izracunajKorekcijuUmL(ocitanPH);
			if(ocitanPH<=mCiljaniPH && vremePoslednjegDoziranja+mMinimalnoVremeIzmedjuDvaDodavanja_s*1000 < trenutnoVreme){
				double potrebnaZapreminaNaOH = mJedinicnoFiksnoDodavanje_mL;
				mDodavanoOdPoslednjeObrade_mS = addML(potrebnaZapreminaNaOH); //povecava i promenjivu mUkupnoDodatoMililitara
				vremePoslednjegDoziranja = trenutnoVreme;

				//korekcija parametarara
				if(mAutomatskoPodesavanjeParametara){
					if(mMinimalnoVremeIzmedjuDvaDodavanja_s<30 && mMinimalnoVremeIzmedjuDvaDodavanja_s+4*1000 > (trenutnoVreme - mVremePoslednjeObrade) ){
						mMinimalnoVremeIzmedjuDvaDodavanja_s++;
						System.out.println("   Povecano mMinimalnoVremeIzmedjuDvaDodavanja_s na: "+mMinimalnoVremeIzmedjuDvaDodavanja_s);
					}
					else
						if(mJedinicnoFiksnoDodavanje_mL > 0.5 && mMinimalnoVremeIzmedjuDvaDodavanja_s*3*1000 > (trenutnoVreme - mVremePoslednjeObrade) ){
							mJedinicnoFiksnoDodavanje_mL -= 0.1;
							System.out.println("   Smanjeno mMinimalnoVremeIzmedjuDvaDodavanja_s na: "+mMinimalnoVremeIzmedjuDvaDodavanja_s);
						}
					//postavlja vrednosti na glavnom ekranu
					mFiksnoDodavanjeLabel.setText(getJedinicnoFiksnoDodavanje_mL().toString());
					mVremeIzmedjDodavanjaLabel.setText(getMinVremeIzmedjuDvaDodavanja_s().toString());
				}
				dodatoOdPoslednjeObrade_mL = mArduinoCore.mS2mL(mDodavanoOdPoslednjeObrade_mS); 
			}

		}
		else{ // za rucni rezim rada
			if(mVremeStartovanjaDoziranja>0){
				if(mVremePoslednjeObrade>mVremeStartovanjaDoziranja){ // proces dodavanja je u dva ili vise intervala obrade
					// obradjuje se poslednji interval - kada se zavrsava doziranje
					if(mAutomatskoDodavanjeDo > mVremePoslednjeObrade 
							&& mAutomatskoDodavanjeDo < trenutnoVreme){
						// dodato u proslom ciklusu
						mDodavanoOdPoslednjeObrade_mS += mAutomatskoDodavanjeDo-mVremePoslednjeObrade;
					}
					else
					{
						mDodavanoOdPoslednjeObrade_mS += trenutnoVreme-mVremePoslednjeObrade;
					}
				}
				else{	
					mDodavanoOdPoslednjeObrade_mS += trenutnoVreme-mVremeStartovanjaDoziranja;
				}
			}

			dodatoOdPoslednjeObrade_mL = mArduinoCore.mS2mL(mDodavanoOdPoslednjeObrade_mS);
			mUkupnoDodatoMililitara += dodatoOdPoslednjeObrade_mL;
		}



		snimiUFajl(ocitanPH, mDodavanoOdPoslednjeObrade_mS,	dodatoOdPoslednjeObrade_mL, mUkupnoDodatoMililitara,
				(mRucniRezimRada ? ", rucnoDoziranje" : ", automatskoDoziranje"));


		mDodavanoOdPoslednjeObrade_mS=0;
		mVremePoslednjeObrade = trenutnoVreme;
	}

	/**
	 * Ukljucuje pumpu
	 */
	public void rucnoDoziranjeUkljuci(){
		mArduinoCore.doziranjeUkljuci();
		mVremeStartovanjaDoziranja = System.currentTimeMillis();
	}

	/**
	 * iskljucuje pumpu
	 */
	public void rucnoDoziranjeIskljuci(){
		long trenutnoVreme = System.currentTimeMillis();
		mArduinoCore.doziranjeIskljuci();

		// u fajl se upisuje na intervalu odbrade, odnosno ako se duze dozira ono se upisuje iz delova
		if(mVremePoslednjeObrade>mVremeStartovanjaDoziranja){
			mDodavanoOdPoslednjeObrade_mS += trenutnoVreme-mVremePoslednjeObrade;
		}
		else{			
			mDodavanoOdPoslednjeObrade_mS += trenutnoVreme-mVremeStartovanjaDoziranja;
		}

		mVremeStartovanjaDoziranja = 0;
	}

	/**
	 * Vraca sta je poslednje snimano u log fajl
	 * @return
	 */
	public String getIzlazPodaci() {
		return mIzlazPodaci;
	}

	public boolean isAutomatskoPodesavanjeParametara() {
		return mAutomatskoPodesavanjeParametara;
	}

	public void setAutomatskoPodesavanjeParametara(boolean mAutomatskoPodesavanjeParametara) {
		this.mAutomatskoPodesavanjeParametara = mAutomatskoPodesavanjeParametara;
	}

	public void definisiLabeleZaAzuriranjeParametara(JLabel pFiksnoDodavanjeLabel, JLabel pVremeIzmedjDodavanjaLabel) {
		mFiksnoDodavanjeLabel = pFiksnoDodavanjeLabel;
		mVremeIzmedjDodavanjaLabel = pVremeIzmedjDodavanjaLabel;
	}

	public void setUkupnoDodataZapremina(Double dodataZapremina) {
		mUkupnoDodatoMililitara = dodataZapremina;

	}

	public void setWorkingMode(WorkingModeEnum mode) {
		mWorkingMode = mode;
	}

	public WorkingModeEnum getWorkingMode() {
		return mWorkingMode;
	}
	
	public double getUkupnoDodatoMililitara() {
		return mUkupnoDodatoMililitara;
	}

	public void setZapreminaSuda(double zapreminaSudaDm3) {
		mZapreminaSuda = zapreminaSudaDm3;
	}

	public Long getMinVremeIzmedjuDvaDodavanja_s() {
		return mMinimalnoVremeIzmedjuDvaDodavanja_s;
	}

	public void setVremeIzmedjuDvaDodavanja_s(long vremeIzmedjuDvaDodavanja_s) {
		this.mMinimalnoVremeIzmedjuDvaDodavanja_s = vremeIzmedjuDvaDodavanja_s;
	}

	public void snimiUFajl(String zaUpisUFajl) {
		try {
			// TODO promeniti nacin definisanja snimanja rezultata
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(mNazivUzorka, true)));
			mIzlazPodaci = new SimpleDateFormat("dd.MM HH.mm.ss.S").format(new Date())+zaUpisUFajl;
			out.println(mIzlazPodaci);
			System.out.println("*"+mIzlazPodaci);			
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void snimiUFajl(double ocitanPH, long dodavanoOdPoslednjeObrade_mS, Double dodatoOdPoslednjeObrade_mL,
			double mUkupnoDodatoMililitara, String string) {
		snimiUFajl(", "+ocitanPH+
				", "+
				dodavanoOdPoslednjeObrade_mS+
				", "+
				dodatoOdPoslednjeObrade_mL+
				", "+
				mUkupnoDodatoMililitara+
				(mRucniRezimRada ? ", rucnoDoziranje" : ", automatskoDoziranje"));

	}

	/**
	 * ukljucuje rucni mod rada
	 * @param bRucniRad
	 */
	public void setRucniRad(boolean bRucniRad) {
		mRucniRezimRada = bRucniRad;
	}

	public String getNameOfSample() {
		return mNazivUzorka;
	}

	public void setNazivUzorka(String nazivUzorka) {
		this.mNazivUzorka = nazivUzorka;
	}

	public void setArduino(ArduinoCore arduinoCore) {
		this.mArduinoCore = arduinoCore;
	}

	public ArduinoCore getArduino() {
		return mArduinoCore;
	}

	public void setTitrtionDose(Double dose) {
		this.mTitrationDose = dose;
	}

}
