package rs.mpele.pHStat;

import java.awt.Dialog.ModalExclusionType;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.fazecast.jSerialComm.SerialPort;

import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ManualPumpControlDialog extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JSpinner spinnerAddml;
	private ArduinoCore mArduino;
	private JSpinner spinnerLoad;
	
	private boolean mbWorking;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ArduinoCore arduino = new ArduinoCore();
					try {
						arduino.konektujSe(SerialPort.getCommPorts()[0].getSystemPortName());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					ManualPumpControlDialog frame = new ManualPumpControlDialog(arduino);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param arduino 
	 */
	public ManualPumpControlDialog(ArduinoCore arduino) {
		mArduino = arduino;
		setTitle("Manual Control " + mArduino.getModuleName());
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setModalExclusionType(ModalExclusionType.TOOLKIT_EXCLUDE);
		setBounds(100, 100, 432, 282);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel);

		JLabel lblSetSpeed = new JLabel("Set speed");
		panel.add(lblSetSpeed);

		final JSpinner spinner = new JSpinner();
		panel.add(spinner);
		spinner.setModel(new SpinnerNumberModel(1000, 100, 9000, 50));

		JButton btnSet = new JButton("Set");
		btnSet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mArduino.komanda("S|"+String.valueOf((int)spinner.getValue()));
				System.out.println("S|"+String.valueOf((int)spinner.getValue()));
			}
		});
		panel.add(btnSet);

		JPanel panel_2 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_2.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel_2);

		JLabel lblAdd = new JLabel("Add");
		panel_2.add(lblAdd);

		spinnerAddml = new JSpinner();
		spinnerAddml.setModel(new SpinnerNumberModel(new Float(1), new Float(0), new Float(20), new Float(0.1)));
		panel_2.add(spinnerAddml);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mArduino.komanda("ADD|"+String.valueOf((int)((float)spinnerAddml.getValue()*1000)));
				System.out.println("ADD|"+String.valueOf((int)((float)spinnerAddml.getValue()*1000)));
			}
		});
		panel_2.add(btnAdd);

		JButton btnAddContinusly = new JButton("Add Continusly");
		btnAddContinusly.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mbWorking == false){					
					mArduino.komanda("PRAZNI");
					mArduino.komanda("GO");
					mbWorking = true;
				}
				else{					
					mArduino.komanda("STOP");
					mbWorking = false;
				}
			}
		});
		panel_2.add(btnAddContinusly);

		JPanel panel_1 = new JPanel();
		FlowLayout flowLayout_2 = (FlowLayout) panel_1.getLayout();
		flowLayout_2.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel_1);

		JLabel lblLoad = new JLabel("Load");
		panel_1.add(lblLoad);

		spinnerLoad = new JSpinner();
		spinnerLoad.setModel(new SpinnerNumberModel(new Float(1), new Float(0), new Float(20), new Float(0.1)));

		panel_1.add(spinnerLoad);

		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mArduino.komanda("LOAD|"+String.valueOf((int)((float)spinnerLoad.getValue()*1000)));
				System.out.println("LOAD|"+String.valueOf((int)((float)spinnerLoad.getValue()*1000)));
			}
		});
		panel_1.add(btnLoad);
		
		JButton btnLoadContiniusly = new JButton("Load Continiusly");
		btnLoadContiniusly.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mbWorking == false){					
					mArduino.komanda("PUNI");
					mArduino.komanda("GO");
					mbWorking = true;
				}
				else{					
					mArduino.komanda("STOP");
					mbWorking = false;
				}
			}
		});
		panel_1.add(btnLoadContiniusly);
	}

}
