package rs.mpele.pHStat;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.fazecast.jSerialComm.SerialPort;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ModulSettingsDialog extends JDialog {
	private static final long serialVersionUID = -3764971464367881132L;
	
	private final JPanel contentPanel = new JPanel();
	private JComboBox<ModuleType> comboBox;
	private JTextField textField_1;
	private JTextField textField_2;

	private ArduinoCore mArduinoCore; 
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ArduinoCore arduino = new ArduinoCore();
			try {
				arduino.konektujSe(SerialPort.getCommPorts()[1].getSystemPortName());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			ModulSettingsDialog dialog = new ModulSettingsDialog(arduino);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * @param arduino 
	 */
	public ModulSettingsDialog(ArduinoCore arduino) {
		mArduinoCore = arduino;
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[][grow]", "[][][]"));
		{
			JLabel lblType = new JLabel("Type");
			contentPanel.add(lblType, "cell 0 0,alignx trailing");
		}
		{
			comboBox = new JComboBox<ModuleType>();
			comboBox.setModel(new DefaultComboBoxModel<ModuleType>(ModuleType.values()));
			contentPanel.add(comboBox, "cell 1 0,growx");
		}
		{
			JLabel lblName = new JLabel("Name");
			contentPanel.add(lblName, "cell 0 1,alignx trailing");
		}
		{
			textField_1 = new JTextField();
			contentPanel.add(textField_1, "cell 1 1,growx");
			textField_1.setColumns(10);
		}
		{
			JLabel lblDescription = new JLabel("Description");
			contentPanel.add(lblDescription, "cell 0 2,alignx trailing");
		}
		{
			textField_2 = new JTextField();
			contentPanel.add(textField_2, "cell 1 2,growx");
			textField_2.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						mArduinoCore.testKonekcije();
						System.out.println(ArduinoCore.prepareModulSettings((ModuleType) comboBox.getSelectedItem(), textField_1.getText(), textField_2.getText()));
						mArduinoCore.setModuleType((ModuleType) comboBox.getSelectedItem());
						mArduinoCore.saveModulSettings(ArduinoCore.prepareModulSettings((ModuleType) comboBox.getSelectedItem(), textField_1.getText(), textField_2.getText()));
						System.out.println(mArduinoCore.procitaj());
//						dispose();
					}
				});
				okButton.setActionCommand("OK");
				getRootPane().setDefaultButton(okButton);
				buttonPane.add(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
