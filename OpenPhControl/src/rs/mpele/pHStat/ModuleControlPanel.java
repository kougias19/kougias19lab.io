package rs.mpele.pHStat;

import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;
import javax.swing.JRadioButton;

public class ModuleControlPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private ArduinoCore mArduinoCore;
	private JComboBox<String> mComboPort;

	private JLabel lblType;

	private JLabel lblName;

	private JLabel lblDescription;

	private JRadioButton rdbtnNewRadioButton;

	/**
	 * Create the panel.
	 * @param arduinoCore 
	 */
	public ModuleControlPanel(ArduinoCore arduinoCore) {
		setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		mArduinoCore = arduinoCore;
		
		setLayout(new MigLayout("", "[][grow,fill]", "[fill][]"));
		
		JLabel lblPort = new JLabel("Port");
		add(lblPort, "flowx,cell 1 0,alignx trailing");

		mComboPort = new JComboBox<String>();
		refreshPortList();
		add(mComboPort, "cell 1 0,growx");
		
		
		final JButton btnConnect = new JButton("Connect");
		btnConnect.setBackground(Color.RED);
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean bKonetovan;
				try {
					bKonetovan = mArduinoCore.konektujSe(mComboPort.getSelectedItem().toString());
					if(bKonetovan){
						btnConnect.setBackground(Color.green);
						lblType.setText("Module type: " + mArduinoCore.getModuleType());
						lblName.setText("Name: " + mArduinoCore.getModuleName());
						lblDescription.setText("Descrtiption: " + mArduinoCore.getModuleDescription());
					}
					else {
						btnConnect.setBackground(Color.RED);
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		add(btnConnect, "cell 1 0");
		
		JButton refreshPortsButton = new JButton("Refresh ports");
		refreshPortsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				refreshPortList();
			}
		});
		add(refreshPortsButton, "cell 1 0");
		
		rdbtnNewRadioButton = new JRadioButton("");
		add(rdbtnNewRadioButton, "cell 0 0 1 2");
		
		
		//////////////////////////////////////////
		JPanel panel = new JPanel();
		panel.setMaximumSize(new Dimension(32767, 300));
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		lblType = new JLabel("Type");
		panel.add(lblType);

		lblName = new JLabel("Name");
		panel.add(lblName);

		lblDescription = new JLabel("Description");
		panel.add(lblDescription);
		add(panel, "flowx,cell 1 1,growx,aligny top");
		
		JPanel panel_1 = new JPanel();
		add(panel_1, "cell 1 1");
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));
		
		
		JButton btnManualControl = new JButton("Manual control");
		panel_1.add(btnManualControl);
		
		
		JButton btnTest = new JButton("ConnectionTest");
		panel_1.add(btnTest);
		btnTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(mArduinoCore.testKonekcije()){
					btnConnect.setBackground(Color.green);
				}
				else {
					btnConnect.setBackground(Color.RED);
				}
			}
		});
		btnManualControl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManualPumpControlDialog frame = new ManualPumpControlDialog(mArduinoCore);
				frame.setVisible(true);
			}
		});
	}
	
	
	private void refreshPortList(){
		mComboPort.removeAllItems();
		for(String nazivPorta : mArduinoCore.ucitajPortove()){
			mComboPort.addItem(nazivPorta);
		}
	}
	
	public JRadioButton getRadioButton(){
		return rdbtnNewRadioButton;
	}


	public ArduinoCore getArduino() {
		return mArduinoCore;
	}
}
