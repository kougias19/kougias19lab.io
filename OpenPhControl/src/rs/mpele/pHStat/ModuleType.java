package rs.mpele.pHStat;

enum ModuleType {
    PUMP_PERISTALTIC(1),
    PUMP_SYRIGE(2);
	
	private int id;

	ModuleType(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }
}