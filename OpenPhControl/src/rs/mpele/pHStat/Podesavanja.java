package rs.mpele.pHStat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Podesavanja {
	private Properties mProperties = new Properties();

	public Properties getProperties() {
		return mProperties;
	}

	public Properties ucitajPodesavanja() throws FileNotFoundException, IOException {
		File f = new File("podesavanja.properties");
		InputStream in = new FileInputStream( f );
		mProperties.load(in);

		return mProperties;
	}


	public void snimiPodesavanjaZaKameru(int x, int y, int treshold, int sirina, int visina, double prethodnaTackaRelativno) throws FileNotFoundException, IOException{

		mProperties.setProperty("x", String.valueOf(x));
		mProperties.setProperty("y", String.valueOf(y));
		mProperties.setProperty("treshold", String.valueOf(treshold));
		mProperties.setProperty("sirina", String.valueOf(sirina));
		mProperties.setProperty("visina", String.valueOf(visina));
		mProperties.setProperty("prethodnaTackaRelativno", String.valueOf(prethodnaTackaRelativno));

		snimi();
	}

	private void snimi() throws FileNotFoundException, IOException{
		mProperties.store(new FileOutputStream("podesavanja.properties"), null);
	}
}
