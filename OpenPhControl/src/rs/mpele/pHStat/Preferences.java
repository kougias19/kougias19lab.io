package rs.mpele.pHStat;

public enum Preferences {
	TYPE (0), NAME (1), DESCRIPTION (2), COEFFICIENT(3);
	
	private int id;

	Preferences(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }
}
