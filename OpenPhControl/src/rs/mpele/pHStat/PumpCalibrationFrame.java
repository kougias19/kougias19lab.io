package rs.mpele.pHStat;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionEvent;

public class PumpCalibrationFrame extends JFrame {

	private static final long serialVersionUID = -3251991239384628949L;
	private JPanel contentPane;
	private ArduinoCore mArduinoCore;
	private JTextField textFieldBrojDoziranja;
	private JTextField textFieldVremeJednogDoziranja;
	private JTextField textFieldPrepumpanaZapremina;
	private JTextField coefficientTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ArduinoCore arduinoCore = new ArduinoCore();
					arduinoCore.konektujSe();

					PumpCalibrationFrame frame = new PumpCalibrationFrame(arduinoCore);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param arduinoCore 
	 */
	public PumpCalibrationFrame(final ArduinoCore arduinoCore) {
		setTitle("Peristaltic pump calibration");
		mArduinoCore = arduinoCore;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 550, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][grow]", "[][][][][][][][]"));

		JLabel label = new JLabel("Number of dosing");
		contentPane.add(label, "cell 0 0,alignx trailing");

		textFieldBrojDoziranja = new JTextField();
		textFieldBrojDoziranja.setText("10");
		contentPane.add(textFieldBrojDoziranja, "flowx,cell 1 0,growx");
		textFieldBrojDoziranja.setColumns(10);

		JLabel label_1 = new JLabel("Time of unit dose (m s)");
		contentPane.add(label_1, "cell 1 0");

		textFieldVremeJednogDoziranja = new JTextField();
		textFieldVremeJednogDoziranja.setText("500");
		contentPane.add(textFieldVremeJednogDoziranja, "cell 1 0");
		textFieldVremeJednogDoziranja.setColumns(10);

		JButton button = new JButton("Dose");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i = 0; i< Integer.valueOf(textFieldBrojDoziranja.getText());i++){
					System.out.println("Doziranje: "+ i );
					try {
						mArduinoCore.dozirajMiliSekundi(Long.valueOf(textFieldVremeJednogDoziranja.getText()));
						TimeUnit.MILLISECONDS.sleep(1000+Integer.valueOf(textFieldVremeJednogDoziranja.getText()));
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		contentPane.add(button, "cell 1 1,alignx right");

		JLabel label_2 = new JLabel("Dosed volume");
		contentPane.add(label_2, "cell 0 2,alignx trailing");

		textFieldPrepumpanaZapremina = new JTextField();
		contentPane.add(textFieldPrepumpanaZapremina, "flowx,cell 1 2,growx");
		textFieldPrepumpanaZapremina.setColumns(10);

		JLabel lblMl = new JLabel("mL");
		contentPane.add(lblMl, "cell 1 2");

		JButton button_1 = new JButton("Calculate");
		button_1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				double rezultatKalibracije = Double.valueOf(textFieldPrepumpanaZapremina.getText())/
						(Double.valueOf(textFieldBrojDoziranja.getText())*Double.valueOf(textFieldVremeJednogDoziranja.getText()));
				coefficientTextField.setText(String.format ("%f", rezultatKalibracije));
			}
		});
		contentPane.add(button_1, "cell 1 3,alignx right");

		coefficientTextField = new JTextField("");
		
		JLabel lblCoefficient = new JLabel("Coefficient");
		contentPane.add(lblCoefficient, "cell 0 4,alignx trailing");
		contentPane.add(coefficientTextField, "flowx,cell 1 4,grow");

		JButton button_2 = new JButton("Save");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				double tmpCoefficient = Double.valueOf(coefficientTextField.getText());
				mArduinoCore.setKoeficijentPumpe(tmpCoefficient);
				mArduinoCore.saveCoefficientPump(tmpCoefficient);
			}
		});

		JLabel lblNewLabel = new JLabel("Old value: "+ mArduinoCore.getKoeficijentPumpe());
		contentPane.add(lblNewLabel, "cell 0 5 2 1");
		contentPane.add(button_2, "cell 1 6,alignx right");
		
		JLabel lblMlms = new JLabel("mL/ms");
		contentPane.add(lblMlms, "cell 1 4");
	}
}
