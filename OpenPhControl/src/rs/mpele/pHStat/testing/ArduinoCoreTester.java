package rs.mpele.pHStat.testing;

import rs.mpele.pHStat.ArduinoCore;

/**
 * 	 Samo ne salje komandu na arduino pa nije potrebno ni konektovanje
 * @author predrag.milanovic
 *
 */
public class ArduinoCoreTester extends ArduinoCore{
	
	/**
	 * Samo ne salje komandu na arduino
	 */
	@Override
	public void komanda(String komanda){
		System.out.println("ArduinoCoreTester - komanda za arduino: "+komanda);
	}
	


}
